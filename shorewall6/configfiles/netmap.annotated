#
# Shorewall6 -- /etc/shorewall6/netmap
#
# For information about entries in this file, type "man shorewall-netmap"
#
# See https://shorewall.org/netmap.html for an example and usage
# information.
#
#############################################################################################
# 
# This file is used to map addresses in one network to corresponding addresses in
# a second network.
# 
# Warning
# 
# To use this file, your kernel and iptables must have NETMAP support included.
# 
# The columns in the file are as follows (where the column name is followed by a
# different name in parentheses, the different name is used in the alternate
# specification syntax).
# 
# TYPE - {DNAT|SNAT}
# 
#     If DNAT, traffic entering INTERFACE and addressed to NET1 has its
#     destination address rewritten to the corresponding address in NET2.
# 
#     If SNAT, traffic leaving INTERFACE with a source address in NET1 has it's
#     source address rewritten to the corresponding address in NET2.
# 
# NET1 - network-address
# 
#     Network in CIDR format (e.g., 192.168.1.0/24). Beginning with Shorewall
#     4.4.24, exclusion is supported.
# 
# INTERFACE - interface
# 
#     The name of a network interface. The interface must be defined in
#     shorewall-interfaces(5). Shorewall allows loose matches to wildcard entries
#     in shorewall-interfaces(5). For example, ppp0 in this file will match a
#     shorewall-interfaces(8) entry that defines ppp+.
# 
# NET2 - network-address
# 
#     Network in CIDR format
# 
# NET3 (Optional) - network-address
# 
#     Added in Shorewall 4.4.11. If specified, qualifies INTERFACE. It specifies
#     a SOURCE network for DNAT rules and a DESTINATION network for SNAT rules.
# 
# PROTO - protocol-number-or-name
# 
#     Optional -- added in Shorewall 4.4.23.2. Only packets specifying this
#     protocol will have their IP header modified.
# 
# DPORT - port-number-or-name-list
# 
#     Optional - added in Shorewall 4.4.23.2. Destination Ports. A
#     comma-separated list of Port names (from services(5)), port numbers or port
#     ranges; if the protocol is icmp, this column is interpreted as the
#     destination icmp-type(s). ICMP types may be specified as a numeric type, a
#     numeric type and code separated by a slash (e.g., 3/4), or a typename. See
#     https://shorewall.org/configuration_file_basics.htm#ICMP.
# 
#     If the protocol is ipp2p, this column is interpreted as an ipp2p option
#     without the leading "--" (example bit for bit-torrent). If no PORT is
#     given, ipp2p is assumed.
# 
#     An entry in this field requires that the PROTO column specify icmp (1), tcp
#     (6), udp (17), sctp (132) or udplite (136). Use '-' if any of the following
#     field is supplied.
# 
#     This column was formerly labelled DEST PORT(S).
# 
# SPORT - port-number-or-name-list
# 
#     Optional -- added in Shorewall 4.4.23.2. Source port(s). If omitted, any
#     source port is acceptable. Specified as a comma-separated list of port
#     names, port numbers or port ranges.
# 
#     An entry in this field requires that the PROTO column specify tcp (6), udp
#     (17), sctp (132) or udplite (136). Use '-' if any of the following fields
#     is supplied.
# 
#     This column was formerly labelled SOURCE PORT(S).
# 
#############################################################################################
#TYPE	NET1		INTERFACE	NET2		NET3		PROTO	DPORT	SPORT
