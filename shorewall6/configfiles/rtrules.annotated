#
# Shorewall6 -- /etc/shorewall6/rtrules
#
# For information about entries in this file, type "man shorewall6-rtrules"
#
# For additional information, see https://shorewall.org/MultiISP.html
#
####################################################################################
# 
# Entries in this file cause traffic to be routed to one of the providers listed
# in shorewall-providers(5).
# 
# The columns in the file are as follows.
# 
# SOURCE (Optional) - {-|[&]interface|address|interface:address}
# 
#     An ip address (network or host) that matches the source IP address in a
#     packet. May also be specified as an interface name optionally followed by
#     ":" and an address. If the device lo is specified, the packet must
#     originate from the firewall itself.
# 
#     Beginning with Shorewall 4.5.0, you may specify &interface in this column
#     to indicate that the source is the primary IP address of the named
#     interface.
# 
#     Beginning with Shorewall 4.6.8, you may specify a comma-separated list of
#     addresses in this column.
# 
# DEST (Optional) - {-|address}
# 
#     An ip address (network or host) that matches the destination IP address in
#     a packet.
# 
#     If you choose to omit either SOURCE or DEST, place "-" in that column. Note
#     that you may not omit both SOURCE and DEST.
# 
#     Beginning with Shorewall 4.6.8, you may specify a comma-separated list of
#     addresses in this column.
# 
# PROVIDER - {provider-name|provider-number|main}
# 
#     The provider to route the traffic through. May be expressed either as the
#     provider name or the provider number. May also be main or 254 for the main
#     routing table. This can be used in combination with VPN tunnels, see
#     example 2 below.
# 
# PRIORITY - priority[!]
# 
#     The rule's numeric priority which determines the order in which the rules
#     are processed. Rules with equal priority are applied in the order in which
#     they appear in the file.
# 
#     1000-1999
# 
#         Before Shorewall-generated 'MARK' rules
# 
#     11000-11999
# 
#         After 'MARK' rules but before Shorewall-generated rules for ISP
#         interfaces.
# 
#     26000-26999
# 
#         After ISP interface rules but before 'default' rule.
# 
#     Beginning with Shorewall 5.0.2, the priority may be followed optionally by
#     an exclaimation mark ("!"). This causes the rule to remain in place if the
#     interface is disabled.
# 
#     Caution
# 
#     Be careful when using rules of the same PRIORITY as some unexpected
#     behavior can occur when multiple rules have the same SOURCE. For example,
#     in the following rules, the second rule overwrites the first unless the
#     priority in the second is changed to 19001 or higher:
# 
#     10.10.0.0/24    192.168.5.6 provider1 19000
#     10.10.0.0/24    -           provider2 19000
# 
# MARK - {-|mark[/mask]}
# 
#     Optional -- added in Shorewall 4.4.25. For this rule to be applied to a
#     packet, the packet's mark value must match the mark when logically anded
#     with the mask. If a mask is not supplied, Shorewall supplies a suitable
#     provider mask.
# 
# Examples
# 
# Example 1:
# 
#     You want all traffic coming in on eth1 to be routed to the ISP1 provider.
# 
#             #SOURCE                 DEST            PROVIDER        PRIORITY      MASK
#             eth1                    -               ISP1            1000
# 
# IPv4 Example 2:
# 
#     You use OpenVPN (routed setup /tunX) in combination with multiple
#     providers. In this case you have to set up a rule to ensure that the
#     OpenVPN traffic is routed back through the tunX interface(s) rather than
#     through any of the providers. 10.8.0.0/24 is the subnet chosen in your
#     OpenVPN configuration (server 10.8.0.0 255.255.255.0).
# 
#              #SOURCE                 DEST            PROVIDER        PRIORITY     MASK
#              -                       10.8.0.0/24     main            1000
# 
####################################################################################
#SOURCE			DEST			PROVIDER	PRIORITY	MASK
