Changes in 5.2.8 Final

1)  Update release documents

2)  Update the %tcdevices documentation at the top of Tc.pm.

3)  Update shorewall-snat(5).

4)  Document NFS macro.

Changes in 5.2.8 RC 1

1)  Update release documents

2)  Rename 'noanycast' to 'omitanycast'

3)  Correct use of $physwild.

4)  Ensure that SHOREWALL_SHELL is set.

5)  Redirect stderr to stdout when using $PAGER

6)  Eliminate duplicate function names.

7)  Don't remove ${SBINDIR}/shorewall when removing Shorewall.

8)  Remove bogus New Feature from release notes.

9)  Include hostname in status command when using Lite product.

10( Display consistent banner from CLI

Changes in 5.2.8 Beta 2

1)  Update release documents

2)  Correct code generated for 'noanycast'.

3)  Flesh out IPv6 anycast documentation.

Changes in 5.2.8 Beta 1

1)  Update release documents

2)  Show filters in output of 'show tc'

3)  Show policing filter in output of 'show classifiers' and
    'shorewall tc'.

4)  Add 'noanycast' interface option.

Changes in 5.2.7 RC 1

1)  Update release documents

2)  Update Shared Config article.

3)  Link the Simple TC article to FAQs 97 and 97a.

Changes in 5.2.7 Beta 1

1)  Update release documents

2)  Add the 'connmark' option in tcdevices.

3)  Support ?FORMAT 2 in the tcpri file.

4)  Merge defect repair from 5.2.6.1.

Changes in 5.2.6 Final

1)  Update release documents

2)  Add the compiler -D option to usage output.

3)  Fix policy chain optimization when EXPAND_POLICIES=No.

Changes in 5.2.6 RC 1

1)  Update release documents

2)  Rename snat PORTS column

3)  Add ?FORMAT 2 support for the snat file.

4)  Merge from 5.2.5.2

5)  Work around iptables --queue-cpu-fanout bug.

Changes in 5.2.6 Beta 1

1)  Update release documents

2)  Implement 'dport' action option

2)  Make 'show actions' more robust

3)  Process the firewall.conf file on Shorewall-lite

Changes in 5.2.5 Final

1)  Update release documents

2)  Zone name too long error message added.

Changes in 5.2.5 RC 1

1)  Update release documents

2)  Update module versions

3)  Omit STATE-oriented rules from wildcard policy chains.

Changes in 5.2.5 Beta 2

1)  Update release documents

2)  Read the params file during 'allow' processing.

3)  Store exported config params in a named array.

4)  Add the 'log' option to the DYNAMIC_BLACKLIST setting.

5)  Add the 'blacklist!' command.

6)  Add the 'noupdate' DYNAMIC_BLACKLIST option.

Changes in 5.2.5 Beta 1

1)  Update release documents

2)  Don't install /etc/network/if-down.d/shorewall on Debian.

3)  Create DBL ipset with 'timeout 0'

Changes in 5.2.4.5

1)  Update release documents

2)  Clarify 'optional' in shorewall-interfaces(5).

3)  Allow AUTOMAKE to work correctly with symbolic links.

Changes in 5.2.4.4

1)  Update release documents

2)  Fix hang during 'shorewall[6] start'

3)  Re-enable automatic creation of dynamic blacklisting ipsets.

Changes in 5.2.4.3

1)  Update release documents

2)  Delete unnecessary check in IPv6 interface_is_usable()

3)  Honor 'wait=<seconds> when enabling an interface.

4)  Don't create the DBL ipset during stop if it doesn't exist

5)  Only destroy the ipsets that will be restored

6)  Load ipsets before stopping firewalls in shorewall-init.

7)  Dramatically speed up optimize level 16.

Changes in 5.2.4.2

1)  Update release documents

2)  Correct handling of 'down' events on Debian where IFUPDOWN=1
    in the Shorewall-init configuration file
    (/etc/default/shorewall-init).

3)  Avoid dupicate 'up' events when a dual-stack interface comes
    on Debian with Shorewall-init IFUPDOWN=1.

Changes in 5.2.4.1

1)  Update release documents

2)  Correct HTTP links to point to the current project website.

3)  Use relative links in the website.

4)  Change URLs to point to the current web site.

5)  Sort specific hash keys and values if -t is specified.

6)  Add cautions to the ipsets document.

7)  Prevent firewalls from being cleared/stopped during OpenSuSE
    upgrades.

8)  Avoid ifupdown extraneous log messages.

Changes in 5.2.4 Final

1)  Update release documents.

2)  Update the IPSets document.

Changes in 5.2.4 RC 1

1)  Update release documents.

2)  Correct QOS Example document's mangle file contents.

3)  Update the Configuration File basics document.

Changes in 5.2.4 Beta 1

1)  Update release documents.

2)  Allow required ICMPv6 packets in stopped state.

3)  Add DOCKER_BRIDGE option in shorewall.conf.

4)  Retire 'trace', 'debug' and 'nolock'.

5)  Remove 'load' from the output of 'shorewall[6] help'

6)  Update the CompiledPrograms.xml article

7)  Replace 'shorewall.net' with 'shorewall.org'.

Changes in 5.2.3 Final

1)  Update release documents.

2)  Correct problem corrected (mention helper).

Changes in 5.2.3 RC 1

1)  Update release documents.

2)  Delete pre-2.6.20 modules from the helpers file

3)  Delete modules* during install

Changes in 5.2.3 Beta 2

1)  Update release documents.

2)  Remove LOAD_HELPERS_ONLY option.

Changes in 5.2.3 Beta 1

1)  Update release documents.

2)  Support zone exclusion in the policy file.

3)  Deprecate all/any[+]-.

4)  Document 'test' argument to compiler.pl

Changes in 5.2.2 Final

1)  Update release documents.

2)  Increase the 'wait' interface option setting limit.

2Changes in 5.2.2 RC 1

1)  Update release documents.

2)  Allow inline matches in the conntrack file.

3)  Tighten check for early matches.

4)  Support '+' in INLINE() accounting rules.

Changes in 5.2.2 Beta 2

1)  Update release documents.

2)  Add comments to the Provider, Zones and Misc Perl modules.

3)  Add NetManager gateway detection.

Changes in 5.2.2 Beta 1

1)  Update release documents.

2)  New macros from Vincas Dargis.

3)  Config.pm cleanup.

4)  Deprecate ULOG.

Changes in 5.2.1.4

1)  Update release documents.

2)  Correct chain name in log messages out of RELATED chains.

3)  Remove dead/silly code in Shorewall::Chains::use_input_chain() and
    Shorewall::Chains::use_output_chain(). Combine the two into a
    single function.

4)  Correct handling of netmask in the RATE column when both a
    source and dest rate are specified.

Changes in 5.2.1.3

1)  Update release documents.

2)  Make 'status -i' work correctly with optional interfaces and no
    providers.

Changes in 5.2.1.2

1)  Update release documents.

2)  Fix an assertion failure during 'check -r' when DOCKER=Yes.

3)  Implement SWCONFDIR upport.

4)  Correct HELPER requires message.

5)  Don't attempt to load ipt_ULOG.

Changes in 5.2.1.1

1)  Update release documents.

2)  Handle emacs issue with generated script.

3)  Correct ip6tables-restore failure message.

4)  Additional fix for 'linkdown' routes.

5)  Accommodate Docker version 18.03.1-ce

Changes in 5.2.1 Final

1)  Update release documents.

2)  Add Eric Teeter's Cockpit macro.

3)  Avoid bad code generation with using SNAT(detect).

Changes in 5.2.1 RC 1

1)  Update release documents.

2)  Apply rate limiting in the nat table rather than in the filter
    table.

3)  Apply fix for Perl 5.23.

Changes in 5.2.1 Beta 3

1)  Update release documents.

2)  Corrected broken links in manpages.

3)  Corrected source interface exclusion.

4)  Correct rate limiting.

5)  Allow shared interfaces to work with statistical load balancing.

6)  Disallow shared optional providers.

Changes in 5.2.1 Beta 2

1)  Update release documents.

2)  Correct typo ( 'fatal-error' => 'fatal_error' ).

3)  Increase verbosity in 'remote-*' operations.

4)  Update version in Shorewall-core to 5.2.

5)  Allow specification of VLSM in the RATE columns.

6)  Allow specification of hash-table buckets and max entries in RATE
    column.

7)  Correct typo in configuration-basics doc.

Changes in 5.2.1 Beta 1

1)  Update release documents.

2)  Add IPFS macros by Răzvan Sandu.

3)  New manpages

4)  Allow interface exclusion.

5)  Allow marking in the NAT table.

6)  Allow more mark/mask freedom with TC_EXPERT=Yes.

Changes in 5.2.0.1

1)  Update release documents.

2)  Merge IfEvent fix from 5.1.12.4.

3)  remote_* changes.

4)  Fix syntax error in the ipdecimal command.

Changes in 5.2.0 Final

1)  Update release documents.

Changes in 5.2.0 RC 2

1)  Update release documents.

2)  Up the INCLUDE depth limit to 20.

3)  Make &lo work correctly.

4)  Drop support for the 'masq' file.

5)  Implement getcaps, getrc, and 'show rc'.

Changes in 5.2.0 RC 1

1)  Update release documents.

2)  Allow AUTOMAKE=<depth>

Changes in 5.2.0 Beta 2

1)  Update release documents.

2)  Correct two-chain case with LOG_ZONE != 'Both'.

3)  Implement RENAME_COMBINED.

4)  Replace ${VARDIR}/firewall with $g_firewall throughout CLI.

5)  Reverse the order of optimize 8 and optimize 16 application.

Changes in 5.2.0 Beta 1

1)  Update release documents.

2)  Remove the MAPOLDACTIONS option.

3)  Remove INLINE_MATCHES.

4)  Remove the 'refresh' command.

5)  Remove deprecated actions and macros.

6)  Update DROP_DEFAULT and REJECT_DEFAULT if Drop and Reject
    respectively.

7)  Convert ';' to ';;' in INLINE and IP[6]TABLES rules.

8)  Add 'show saves' command.

9)  Add shorewallrc.sandbox

10) Implement LOG_ZONE.
